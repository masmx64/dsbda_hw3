#!/bin/bash

echo "Start Elasticsearch"

su grey -c "./elasticsearch-6.1.0/bin/elasticsearch -d"

while [ ! "$(curl -X GET 'http://localhost:9200' 2>/dev/null)" ]; do
  sleep 1
done

echo "Done"


echo "Start Logstash [background]"

su grey -c "./logstash-6.1.0/bin/logstash -f '/home/grey/logstash.conf' 1>/dev/null &"


echo "Start Grafana"

cd /usr/share/grafana
/usr/sbin/grafana-server 1>/dev/null &
cd /home/grey

while [ ! "$(curl -X GET 'http://admin:admin@localhost:3000/api/datasources' 2>/dev/null)" ]; do
  sleep 1
done

curl -X POST 'http://admin:admin@localhost:3000/api/datasources' -H "Content-Type: application/json"  --data-binary '{"name":"twitter","type":"elasticsearch","url":"http://localhost:9200","access":"proxy","database":"twitter","jsonData":{"esVersion":5,"timeField":"@timestamp"}}' > /dev/null 2>&1

#curl 'http://admin:admin@localhost:3000/api/dashboards/db' -H "Content-Type: application/json"  --data-binary '{"dashboard":{"id":null,"title":"Twitter","tags": ["templated"],"timezone": "browser","rows":[{}],"schemaVersion":6,"version":0},"overwrite":false}'

#curl 'http://admin:admin@localhost:3000/api/dashboards/import' -H "Content-Type: application/json" --data-binary "@dashboard.json"

echo "Done"


java -jar ./TwitterLog/target/TwitterLog-1.0-jar-with-dependencies.jar /home/grey/twitter.log 60
