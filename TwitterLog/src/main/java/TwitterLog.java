
import java.io.Serializable;
import java.util.*;
import java.text.*;
import java.io.*;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.TwitterStream;
import twitter4j.*;

import java.util.*;
import java.text.*;

import java.util.concurrent.TimeUnit;


/**
	TwitterLog
	@author Varykhanov S.S.
	@version 1.0
*/
public class TwitterLog
{
	// Keys	
	public final static String ConsumerKey = "wo71U5Atj5xhbAoFJEwbUu7N4";
	public final static String ConsumerSecret = "EYOxWvRjC1awzfY6u1BgJKtp5Q2tuhX5VqxhxzdLbf4ZNvYFf4";
	public final static String AccessToken = "941005842123575296-CkjeXLm1uxXml6JQxUGgc5nhrGhKAnm";
	public final static String AccessTokenSecret = "5aibWjGlNP65tCelInZuZKQ8YKEFLa1gnnD5V57V9bZno";

	public final static DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");


	public static int count;
	public static int time_now;
	public static int time_last;
	public static boolean first;
	public static String log_path;
	public static long interval;
	public static Date date;
	public static TwitterStream twitterStream;


	/**
		Main function
	*/
	public static void main( String[] args ) throws IOException
	{

		// Read args		
		if( args.length != 2 )
		{
			System.out.println( "Wrong arguments count" );	
			System.exit(1);
		}	

		log_path = args[0];

		try
		{
			interval = Integer.parseInt( args[1] );
		}
		catch(NumberFormatException e)
		{
			System.out.println( "Wrong interval" );
			System.exit(1);
		}

		// Start listen stream
		System.out.println("===== START =====");

		startStream();		

		// Wait stop
        System.out.println("Press Enter key to stop");
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {} 

		// Stop stream
		twitterStream.cleanUp();
		twitterStream.shutdown();

	}


	/**
		Stream function
	*/
	public static void startStream()
	{

		// Listener		
		StatusListener listener = new StatusListener()
		{
			@Override
		    public void onStatus( Status status ){}

		    @Override
		    public void onDeletionNotice( StatusDeletionNotice statusDeletionNotice )
			{
				date = Calendar.getInstance().getTime();
				time_now = (int)(date.getTime()/1000L/interval);
	
				// Read one delete event
				deleteEvent();
		    }

		    @Override
		    public void onTrackLimitationNotice( int numberOfLimitedStatuses ){}

		    @Override
		    public void onScrubGeo( long userId, long upToStatusId ){}

		    @Override
		    public void onStallWarning( StallWarning warning ){}

		    @Override
		    public void onException( Exception ex )
			{
				ex.printStackTrace();
		    }
		};


		// Setupe config
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
			.setOAuthConsumerKey( ConsumerKey )
			.setOAuthConsumerSecret( ConsumerSecret )
			.setOAuthAccessToken( AccessToken )
			.setOAuthAccessTokenSecret( AccessTokenSecret );


		// Create stream object
        twitterStream = new TwitterStreamFactory(cb.build()).getInstance();

        twitterStream.addListener( listener );

		// Init time
		time_last = (int)(Calendar.getInstance().getTime().getTime()/1000L/interval);

		count = 0;
		first = true;

		// Start reading
        twitterStream.sample();

	}


	/**
		On delete event handler
		increment count and write count
		to log file if interval is left
	*/
	public static void deleteEvent()
	{
		if( time_now > time_last )
		{
			if( !first )
			{
				try						
				{
					PrintWriter writer = new PrintWriter(new FileOutputStream(new File( log_path ), true)); 							writer.println( df.format( date ) + " " + count );
					writer.close();
				}
				catch(IOException e)
				{
					System.out.println( "Cant write to log" );
					System.exit(1);
				}

				System.out.println( df.format( date ) + " " + count );
			}
			else
				first = false;

			time_last = time_now;
			count = 0;
		}

		count++;
	}
	
}














