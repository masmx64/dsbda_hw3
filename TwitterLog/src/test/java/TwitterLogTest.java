
import java.io.Serializable;
import java.util.*;
import java.text.*;
import java.io.*;
import java.nio.file.*;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.TwitterStream;
import twitter4j.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.util.*;
import java.text.*;
import org.junit.*;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;


/**
	Unit tests class
	@author Varykhanov S.S.
	@version 1.0
*/
public class TwitterLogTest
{

	/**
		Test TwitterLog.deleteEvent
		Normal work
	*/
    @Test
    public void test_deleteEvent_0()
	{
		TwitterLog.count = 100;
		TwitterLog.first = false;
		TwitterLog.log_path = "/home/grey/twitter_d0.log";
		TwitterLog.interval = 60;

		TwitterLog.date = Calendar.getInstance().getTime();
		TwitterLog.time_now = (int)(TwitterLog.date.getTime()/1000L/TwitterLog.interval);
		TwitterLog.time_last = TwitterLog.time_now - 1;

		TwitterLog.deleteEvent();

		try(BufferedReader br = new BufferedReader(new FileReader(TwitterLog.log_path)))
		{
			String line = br.readLine();

			try {
				Files.delete( Paths.get(TwitterLog.log_path) );
			} catch (FileSystemException x) {
				fail("Delete file");
			} catch (IOException x) {
				fail("Delete file");
			}

			if( !line.equals( TwitterLog.df.format( TwitterLog.date ) + " " + 100))
				fail("Invalid value");

		}catch(java.io.FileNotFoundException e){ fail("file not found"); }
		catch(java.io.IOException e){ fail("IO"); }
	}


	/**
		Test TwitterLog.deleteEvent
		Skip first
	*/
    @Test
    public void test_deleteEvent_1()
	{
		TwitterLog.count = 100;
		TwitterLog.first = true;
		TwitterLog.log_path = "/home/grey/twitter_d1.log";
		TwitterLog.interval = 60;

		TwitterLog.date = Calendar.getInstance().getTime();
		TwitterLog.time_now = (int)(TwitterLog.date.getTime()/1000L/TwitterLog.interval);
		TwitterLog.time_last = TwitterLog.time_now - 1;

		TwitterLog.deleteEvent();

		TwitterLog.time_last = TwitterLog.time_now - 1;
		
		TwitterLog.deleteEvent();

		try(BufferedReader br = new BufferedReader(new FileReader(TwitterLog.log_path)))
		{
			String line = br.readLine();

			try {
				Files.delete( Paths.get(TwitterLog.log_path) );
			} catch (FileSystemException x) {
				fail("Delete file");
			} catch (IOException x) {
				fail("Delete file");
			}

			//System.out.println("1>"+line);
			//System.out.println("2>"+TwitterLog.df.format( TwitterLog.date ) + " " + 1);
			if( !line.equals( TwitterLog.df.format( TwitterLog.date ) + " " + 1))
				fail("Invalid value");

		}catch(java.io.FileNotFoundException e){ fail("file not found"); }
		catch(java.io.IOException e){ fail("IO"); }
	}

	
	/**
		Test TwitterLog.startStream
		Start read stream and check data
	*/
	@Test
    public void test_startStream()
	{
		TwitterLog.count = 0;
		TwitterLog.time_now = 101;
		TwitterLog.time_last = 100;
		TwitterLog.first = true;
		TwitterLog.log_path = "/home/grey/twitter_d10.log";
		TwitterLog.interval = 1;

		TwitterLog.date = Calendar.getInstance().getTime();
		TwitterLog.time_last = (int)(TwitterLog.date.getTime()/1000L/TwitterLog.interval);

		TwitterLog.startStream();
		
		try
		{		
			TimeUnit.SECONDS.sleep( 15 );
		}
		catch(InterruptedException e){ fail("OMG!!"); };

		TwitterLog.twitterStream.cleanUp();
		TwitterLog.twitterStream.shutdown();


		try(BufferedReader br = new BufferedReader(new FileReader(TwitterLog.log_path)))
		{
			if( br.readLine() == null )
				fail("File is empty");

		}catch(java.io.FileNotFoundException e){ fail("file not found"); }
		catch(java.io.IOException e){ fail("IO"); }

		try {
			Files.delete( Paths.get(TwitterLog.log_path) );
		} catch (FileSystemException x) {
			fail("Delete file");
		} catch (IOException x) {
			fail("Delete file");
		}
	}


	/**
		Test full chain
		1) Create simple log file
		2) File => Logstash => Elasticsearch
		3) GET index and try find record
	*/
	@Test
	public void test_allChain()
	{
		TwitterLog.count = 123456;
		TwitterLog.first = false;
		TwitterLog.log_path = "/home/grey/twitter_d.log";
		TwitterLog.interval = 60;

		TwitterLog.date = Calendar.getInstance().getTime();
		TwitterLog.time_now = (int)(TwitterLog.date.getTime()/1000L/TwitterLog.interval);
		TwitterLog.time_last = TwitterLog.time_now - 1;

		TwitterLog.deleteEvent();


		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet("http://localhost:9200/twitter_d/_search");
		HttpResponse response;

		int i;
		for( i=0; i<50; i++ )
		{

			String line = "";

			try {
				response = client.execute(request);

				// Get the response
				BufferedReader br;

				br = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));

				line = br.readLine();
				System.out.println(line);

			} catch (IOException e) {
				e.printStackTrace();
			} catch (UnsupportedOperationException e) {
				e.printStackTrace();
			}

			if( line.contains( "123456" ) )
				break;

			try
			{		
				TimeUnit.SECONDS.sleep( 5 );
			}
			catch(InterruptedException e){ fail("OMG!!"); };

		}

		if( i==50 )
			fail(">>> Message not found <<<");

		try {
			Files.delete( Paths.get(TwitterLog.log_path) );
		} catch (FileSystemException x) {
			fail("Delete file");
		} catch (IOException x) {
			fail("Delete file");
		}
	}


}














