
FROM ubuntu:16.04

# Forward port
EXPOSE 3000

# Install package
RUN apt-get update
RUN apt-get install -y wget curl tar openjdk-8-jdk maven apt-transport-https

# Create user
WORKDIR /home/grey
RUN useradd -d /home/grey grey
RUN chown grey .

# Download Elasticsearch
RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.1.0.tar.gz
RUN tar -xzf elasticsearch-6.1.0.tar.gz
RUN rm elasticsearch-6.1.0.tar.gz
RUN chown grey -R elasticsearch-6.1.0

# Dowload logstash
RUN wget https://artifacts.elastic.co/downloads/logstash/logstash-6.1.0.tar.gz
RUN tar -xzf logstash-6.1.0.tar.gz
RUN rm logstash-6.1.0.tar.gz
RUN chown grey -R logstash-6.1.0

# Download && install Grafana
RUN apt-get install -y adduser libfontconfig
RUN wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_4.6.3_amd64.deb
RUN dpkg -i grafana_4.6.3_amd64.deb
RUN rm grafana_4.6.3_amd64.deb

# Build project
COPY TwitterLog /home/grey/TwitterLog
COPY test_env.sh /home/grey/test_env.sh
COPY logstash_d.conf /home/grey/logstash_d.conf
RUN ./test_env.sh && cd TwitterLog && mvn package assembly:single
RUN chown grey -R TwitterLog

# Copy logstash config
COPY logstash.conf /home/grey/logstash.conf

# Copy dashboard
#COPY dashboard.json /home/grey/dashboard.json

# Copy main script
COPY main.sh /home/grey/main.sh

# Copy log for example
COPY twitter.log /home/grey/twitter.log
RUN chown grey twitter.log

#Set not root user
#USER grey

# Run command
CMD ["/home/grey/main.sh"]
