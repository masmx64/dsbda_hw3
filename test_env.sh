#!/bin/bash

echo "Start Elasticsearch"

su grey -c "./elasticsearch-6.1.0/bin/elasticsearch -d"

while [ ! "$(curl -X GET 'http://localhost:9200' 2>/dev/null)" ]; do
  sleep 1
done

echo "Done"


echo "Start Logstash [background]"

su grey -c "./logstash-6.1.0/bin/logstash -f '/home/grey/logstash_d.conf' 1>/dev/null &"
